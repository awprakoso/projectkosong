import React from 'react';

import DashboardStack from './DashboardStack';
import News from '../screens/News';
import TodoScreen from '../screens/TodoScreen';
import Interest from '../screens/Interest';


import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
const Tab = createBottomTabNavigator();

export default function MainNavigator(){
    return(
        <>
        <Tab.Navigator
          screenOptions = {({ route }) => ({
            tabBarIcon: ({ focused, color, size }) => {
              let iconName;

              if (route.name === 'News'){
                iconName = focused ? 'desktop-outline' : 'desktop'
              } else if (route.name === 'Dashboard' ){
                iconName = focused ? 'home-outline' : 'home'
              } else if (route.name === 'Todo'){
                iconName = focused ? 'person-circle-outline' : 'person-circle'
              } else if (route.name === 'Skills'){
                iconName = focused ? 'barbell-outline' : 'barbell'
              }
              return <Ionicons name={iconName} size={size} color={color} />
            }
          })}
          tabBarOptions={{
            activeTintColor: 'cornflowerblue',
            inactiveTintColor: 'gray'
          }}
          initialRouteName='Dashboard'
        >
          <Tab.Screen name='News' component={News} />
          <Tab.Screen name='Dashboard' component={DashboardStack} />
          <Tab.Screen name='Todo' component={TodoScreen} />
          <Tab.Screen name='Skills' component={Interest} />
        </Tab.Navigator>
      </>
    )
}