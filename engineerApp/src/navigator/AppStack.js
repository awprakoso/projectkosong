import React from 'react';
import LoginScreen from '../screens/LoginScreen';
import RegisterScreen from '../screens/RegisterScreen';
import MainNavigator from '../navigator/MainNavigator';

import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
import {connect} from 'react-redux';

const Stack = createStackNavigator()

function AppStack(props) {
    return(
        <NavigationContainer>
            <Stack.Navigator>
                {props.hasLoggedIn ? (
                <Stack.Screen options={{headerShown: false}} name='Main' component={MainNavigator}/>
                ) : (
                <>
                    <Stack.Screen name='Login' component={LoginScreen}/>
                    <Stack.Screen name='Register' component={RegisterScreen}/>
                </>
                )}
            </Stack.Navigator>
        </NavigationContainer>
    )
}

const mapStateToProps = (state) => ({
    hasLoggedIn: state.auth.isLoggedIn
})

const mapDispatchToProps = (dispatch) => ({
   
})
export default connect(mapStateToProps, mapDispatchToProps)(AppStack);