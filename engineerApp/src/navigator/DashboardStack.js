import React from 'react';
import Dashboard from '../screens/dashboard';
import Dummy from '../screens/DummyPage';
import {createStackNavigator} from '@react-navigation/stack';

const Stack = createStackNavigator()

export default function DashboardStack(){
    return(
        <Stack.Navigator>
            <Stack.Screen options={{headerShown: false}} name='Dashboard' component={Dashboard}/>
            <Stack.Screen name='Dummy' component={Dummy}/>
        </Stack.Navigator>
    )
}
