import {LOGIN, LOGOUT} from './auth_types';

export const loginAction = (payload) => {
    return {type: LOGIN, payload}
};

export const logoutAction = () => {
    return {type: LOGOUT};
};