import { GET_INTEREST } from './interest_types';

export const getInterests = () => {
    return {type: GET_INTEREST};
};