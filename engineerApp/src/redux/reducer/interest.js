import {GET_INTEREST_SUCCESS, GET_INTEREST_FAILED} from '../action/interest_types';


//initial state = nilai awal data profile yang ada di store
const initialState = [];

const interest = (state = initialState, action) => {
    switch (action.type){
        case GET_INTEREST_SUCCESS: {
            return [
                ...state,
                ...action.payload.interests
            ]
        }
        case GET_INTEREST_FAILED: {
            return []
        }
        default:
            return state;
    }
};

export default interest;