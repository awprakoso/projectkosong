import {LOGIN, LOGIN_SUCCESS, LOGIN_FAILED, LOGOUT} from '../action/auth_types'
//src/reducer/auth.js
const initialState = {
    isLoading: false,
    isLoggedIn: false,
};

const auth = (state = initialState, action) => {
    switch (action.type){
        case LOGIN:{
            //when login button is clicked
            return {
                ...state,
                isLoggedIn: true
            }
        }
        case LOGIN_SUCCESS:{
            //login succeeds
            return {
                ...state,
                isLoggedIn: true,
                isLoading: false,
            }
        }
        case LOGIN_FAILED: {
            //login failed
            return {
                ...state,
                isLoading: false,
            }
        }
        case LOGOUT: {
            //when logout button is clicked
            return {
                ...state,
                isLoggedIn: false,
                isLoading: false,
            } 
        }
        default:
            return state;
    }
};

export default auth;