import {put, takeLatest} from 'redux-saga/effects';
import axios from 'axios';
import { ToastAndroid } from 'react-native';

function* register(action){
    try{
        const resRegister = yield axios({
            method: 'POST',
            url: 'http://localhost:3000/auth/register',
            data: action.payload
        });

        if (resRegister && resRegister.data) {
            //save data to store (reducer)
            yield put({
                type: 'REGISTER_SUCCESS',
                username: resRegister.data.username,
            }); //<-- dispatch({type: 'REGISTER_SUCCESS})

            //show message berhasil
            ToastAndroid.showWithGravity(
                'Registered successfully',
                ToastAndroid.SHORT,
                ToastAndroid.BOTTOM,
            )
        } else {
            //show message error
            ToastAndroid.showWithGravity(
                'Failed to register',
                ToastAndroid.SHORT,
                ToastAndroid.BOTTOM,
            )
        }
    } catch(err){
        ToastAndroid.showWithGravity(
            'Failed to register',
            ToastAndroid.SHORT,
            ToastAndroid.BOTTOM,
        )
    }
}

function* authRegisterSaga(){
    yield takeLatest('REGISTER', register);
}

export default authRegisterSaga;