import {all} from 'redux-saga/effects';
import authSaga from './auth';
import profileSaga from './profile';
import interestSaga from './interest'

export default function* rootSaga() {
  yield all([authSaga(), profileSaga(), interestSaga()]);
}
