import {takeLatest, put} from 'redux-saga/effects';
import {apiFetchInterest} from '../../common/api/interest';
import {ToastAndroid} from 'react-native';
import {getHeaders, getAccountId} from '../../common/function/auth';
import {GET_INTEREST, GET_INTEREST_SUCCESS, GET_INTEREST_FAILED} from '../action/interest_types';

// import AsyncStorage from '@react-native-async-storage/async-storage';

function* getInterests() {
  try {
    const headers = yield getHeaders();
    const accountId = yield getAccountId();

    // FETCHING INTEREST
    console.info('1');
    const resInterest = yield apiFetchInterest(accountId, headers);
    yield put({type: GET_INTEREST_SUCCESS , payload: resInterest.data})
    console.info('2');
  } catch (e) {
      ToastAndroid.showWithGravity(
        `Failed to fetch interests data`,
        ToastAndroid.SHORT,
        ToastAndroid.BOTTOM,
      );

      yield put({type: GET_INTEREST_FAILED});
  }
}

function* interestSaga() {
  yield takeLatest(GET_INTEREST, getInterests);
}

export default interestSaga;
