import axios from 'axios';

export function apiFetchProfileDetail(id, headers) {
  return axios({
    method: 'GET',
    url: 'http:/localhost:3000/profile/' + id,
    headers,
  });
}
