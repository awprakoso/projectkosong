import React, {useState} from 'react';
import {View, ActivityIndicator, TextInput, Text, TouchableOpacity, Button} from 'react-native';
import { connect } from 'react-redux';
import { loginAction } from '../redux/action/auth';

function LoginScreen(props){
    const [username, setUsername] = useState('demo');
    const [password, setPassword] = useState('demo123');
    const [message, setMessage] = useState(null);

    const login = () => {
        if (!username){
            setMessage('Username is empty!');
        } else if (!password){
            setMessage('Password is empty!');
        } else {
            props.processLogin({username, password});
        }
    }

    return(
        <View>
            <Text>LOGIN HERE</Text>

            <Text style={{color: 'red'}}>{message}</Text>

            <TextInput 
            placeholder='Username' 
            value={username} 
            onChangeText={(text) => setUsername(text)}
            />
            <TextInput 
            placeholder='Password' 
            value={password} 
            onChangeText={(text) => setPassword(text)}
            secureTextEntry
            />

            {props.isLoading ? (
                <TouchableOpacity
                  onPress={() => console.info('disabled')}
                  style={{borderRadius: 50, backgroundColor: '#f2f2f2', padding: 15}}>
                  <ActivityIndicator size="large" color="#333333" />
                </TouchableOpacity>
              ) : (
                <TouchableOpacity
                  onPress={() => login()}
                  style={{borderRadius: 50, backgroundColor: '#444', padding: 15}}>
                  <Text style={{textAlign: 'center', color: '#fff'}}>LOGIN</Text>
                </TouchableOpacity>
            )}

            <TouchableOpacity
                style={{marginTop: 20, padding: 10}}
                onPress={() => props.navigation.navigate('Register')}>
                <Text style={{textAlign: 'center', color: 'black'}}>Don't have an account? Create one!</Text>
            </TouchableOpacity>
            
        </View>
    )
}

const mapStateToProps = (state) => ({
    isLoading: state.auth.isLoading,
})

const mapDispatchToProps = (dispatch) => ({
    processLogin: (data) => dispatch(loginAction(data))
})
export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);