import React from 'react';

import {View, Button, StyleSheet, Text, image} from 'react-native';
import { connect } from 'react-redux';
import WeatherList from '../components/Weather.component';
import JokesList from '../components/Jokes.component';
import PromotionsList from '../components/Promotions.component';

import {logoutAction} from '../redux/action/auth';
import Ionicons from 'react-native-vector-icons/Ionicons';

class Dashboard extends React.Component{
    constructor(props){
        super(props);
        this.state = {};
    }
    
    componentDidMount() {}

    render(){ 
        return (
            <View>

                {/** HEADER */}
                <View style={{flexDirection: 'row'}}>
                    <View style={{flex: 1, flexDirection: 'row'}}>
                    {/*<Image
                        source={{uri: this.props.photo}}
                        style={{width: 40, height: 40, borderRadius: 50}}
                    />*/}

                    <View style={{marginLeft: 10}}>
                        <Text style={{fontWeight: 'bold'}}>
                        Hi, {this.props.fullname}
                        </Text>
                        <Text style={{fontWeight: 'bold'}}>@{this.props.username}</Text>
                    </View>
                    </View>

                    <View style={{flex: 1, alignItems: 'flex-end'}}>
                    <Ionicons name="notifications" size={30} color="#444" />
                    </View>
                </View>
               
                <View style = {styles.objects}>
                    <PromotionsList/>
                    <JokesList />
                    <WeatherList />

                    <View style={{marginTop: 50}}>
                    {this.props.totalTodos === 0 ? (
                        <Text>Belum ada tugas</Text>
                        ) : (
                        <Text>
                        Masih ada {this.props.totalTodos} tugas yang harus dikerjakan
                        </Text>
                    )}
                    </View>

                    <Button 
                    onPress={() => this.props.navigation.navigate('Dummy')}
                    title='Go to the Dummy Page'/>
                    <Button onPress={() => this.props.processLogout()} title="LOGOUT" /> 
                </View>
                
            </View>
        )
    }
}


const styles = StyleSheet.create({
    objects:{
        display: 'flex',
        padding: 10
    }
})

const mapStateToProps = (state) => ({
    fullname: state.profile.first_name + ' ' + state.profile.last_name,
    photo: state.profile.photo_url,
    username: state.profile.username,
    totalTodos: state.todos.length,
  });
  
  const mapDispatchToProps = (dispatch) => ({
    processLogout: () => dispatch(logoutAction()),
  });
  
  export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);