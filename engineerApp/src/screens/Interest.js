import React from 'react'
import {View, Text} from 'react-native';
import { connect } from 'react-redux';

function InterestComponent(props) {
    return (
        <View>
            {/**this is the list of interests */}
            <View>
                <Text>UPGRADE YOUR SKILLS</Text>
                <View>{props.interests}</View>
            </View>
               
            {/**this is the contents based on interest */}
            <View>
                <Text>CONTENTS FOR YOU</Text>
            </View>
        </View>
    )
}

const mapStateToProps = (state) => ({
    interests: state.interest.interests
})

const mapDispatchToProps = (dispatch) => ({})

export default connect(mapStateToProps, mapDispatchToProps)(InterestComponent)