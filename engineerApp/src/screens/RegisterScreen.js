import React, {useState} from 'react';
import {View, TextInput, Button, Text} from 'react-native';
import { connect } from 'react-redux';

function RegisterScreen(props){
    const [username, setUsername] = useState(null);
    const [password, setPassword] = useState(null);
    const [firstName, setFirstName] = useState(null);
    const [lastName, setLastName] = useState(null);
    const [message, setMessage] = useState(null);

    //const [nama_state, nama_fungsi_untuk_]

    const register = () => {
        if (!username){
            setMessage('Username is required')
        } else if (!password){
            setMessage('Password is required')
        } else if (!firstName){
            setMessage('First name is required')
        } else {
            setMessage(null);
            //process Login
            const dataRegister = {
                username: username,
                password: password,
                first_name, firstName,
                last_name, lastName
            }
            props.processRegister(dataRegister)
        }
    }

    return (
        <View>
            <TextInput 
            placeholder="Username*"
            onChangeText={(text) => setUsername(text)}
            value={username}
            />
            <TextInput 
            placeholder="Password*"
            onChangeText={(text) => setPassword(text)}
            value={password}
            />
            <TextInput 
            placeholder="First Name*"
            onChangeText={(text) => setFirstName(text)}
            value={firstName}
            />
            <TextInput 
            placeholder="Last Name"
            onChangeText={(text) => setLastName(text)}
            value={lastName}
            />
            <Text style={{color: 'gray', fontStyle: 'italic'}}>*is required</Text>

            <Text style={{color: 'red', fontStyle: 'italic'}}>{message}</Text>
            <Button
            title="Sign Up"
            onPress= {() => register()}
            />
        </View>
    )
}

const mapStateToProps = (state) => ({
    //
})

const mapDispatchToProps = (dispatch) => ({
    processRegister: (data) => dispatch({type: 'REGISTER', payload: data})
})

export default connect(mapStateToProps, mapDispatchToProps)(RegisterScreen)