import React from 'react';
import axios from 'axios';
import { ScrollView, View, Text, Image, StyleSheet } from 'react-native';
import NewsItem from '../component/NewsItem.component';

export default class News extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            listNews: []
        }
    }

    componentDidMount(){ //dipanggil hanya sekali ketika halaman dibuka
        //fetch data from url 
        axios({
          method: 'GET', //POST, PATCH, PUT, DELETE
          url: 'http://newsapi.org/v2/top-headlines?sources=techcrunch&apiKey=c2232c39155942149caa73702b1fc6f2'
        })
        .then((res) => {
          //when success
          console.info('articles', res.data.articles.length);
          this.setState({ listNews: res.data.articles });
        })
        .catch((err) => {
          //when error
        })
      }

    render(){
        return (
            <View style={styles.container}>
                <View>
                    <Text style={styles.Header}>News For You, Baby</Text>
                </View>

                <ScrollView style={styles.scrollView}>
                    {this.state.listNews.map((news, index) => (
                        <NewsItem 
                        key={index}
                        title={news.title}
                        author={news.author}
                        image={news.urlToImage}
                        description={news.description}
                        date={news.publishedAt}
                        />
                    ))}
                </ScrollView>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container:{
        marginBottom: 50
    },

    Header:{
        textAlign: 'center',
        fontFamily: 'Poppins', 
        fontSize: 20, 
        fontWeight: 'bold',
        borderColor: 'black',
        borderBottomWidth: 2
    },
  
    scrollView:{
        backgroundColor: 'lightcyan'
    }
  })