import React from 'react';
import { View, Text, Image, StyleSheet} from 'react-native';

export default class NewsItem extends React.Component{
    constructor(props){
        super(props)
    }

    render(){
        return(
            <View style={styles.container}>
                <View>
                    <Image source={{uri: this.props.image}} style={styles.image} />
                </View>

                <View style={styles.content}>
                    <View>
                        <Text style={styles.title}>{this.props.title} </Text>
                    </View>
                    <View>
                        <Text style={styles.authorDate}>{this.props.author} | {this.props.date}</Text>
                    </View>
                    <View>
                        <Text>{this.props.description}</Text>
                    </View>
                </View>
                
            </View>
        )
        
    }
}

const styles = StyleSheet.create({
    container:{
        display: 'flex',
        flexDirection: "row",
        // margin: 10,
        borderColor: 'lightskyblue',
        borderTopWidth: 2,
        borderBottomWidth: 2
    },
    image:{
        flex: 1,
        width: 100, 
        height: 100
    },
    content:{
        flex: 3,
        padding: 10        
    },
    title:{
        fontWeight: 'bold'
    },
    authorDate:{
        fontSize: 12,
        color: 'gray'
    }
})